$(function () {

    $(".product-thumb__item").hover(function () {
        var src = $(this).attr("data-src");
        $(".featured-image img").prop("src", src);
        $(this).css("border", "1px solid red");

    },
        function () {
            $(this).css("border", "0px solid red");
            $(this).css("box-shadow", "none");
        }
    );
    $(".featured-image").on(
        {
            mouseenter: function () {
                $(this).css("transform", "scale(1.5)");
            },
            mouseleave: function () {
                $(this).css("transform", "scale(1)");
            }
        });

});