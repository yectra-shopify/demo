$(function() {
    $(".product-thumb__item").on("mouseover", function() {
        var src = $(this).attr("data-src");

        $(".featured-image img").prop("src", src);
    });
});